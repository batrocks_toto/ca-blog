<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		require_once APPPATH.'third_party/src/Google_Client.php';
		require_once APPPATH.'third_party/src/contrib/Google_Oauth2Service.php';
		require_once APPPATH.'third_party/src/Faker/autoload.php';

		$this->load->model('post_model', 'pm');
	}

	public function index()
	{
		$data['user_id'] = '';

		$per_page = 5;
		$page = ($this->input->get('page')) ? $this->input->get('page') : 0;
		$view = 'content_view';
		$id = null;
		if(empty($_SESSION['token'])){
			$data['title_header'] = 'Recent Blogs';
		}
		else{
			$id =  $_SESSION['user']['id'];
			$data['user_id'] = $id;
        	$data['form_action'] = 'post';
        	$data['title_header'] = 'Your Recent Blogs';
        	$view = 'account_view';
		}

		$data['posts'] =  $this->pm->getposts($page, $per_page, $id);
		$total_rows = $this->pm->numberofitems($id);

		$data['links'] = $this->pagination_lib->generate_pagination_links($total_rows, 'home');
		$data['posts_view'] = $this->load->view('posts_view', $data, true);

		if (!$this->input->is_ajax_request()) {
		   $this->template_lib->set_view('index_view', $view, $data,'',$data);
		} else {
			echo json_encode($data);
		}
	}
}
