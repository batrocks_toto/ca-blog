<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		require_once APPPATH.'third_party/src/Google_Client.php';
		require_once APPPATH.'third_party/src/contrib/Google_Oauth2Service.php';

		$this->load->library('pagination');
		$this->load->model('post_model', 'pm');
		//$this->load->model('pagination_model', 'ppm');
	}

	public function index()
	{

		$clientId = '100324004017-0tth54o8qdta4p7ph14uqkb9i6ms8ald.apps.googleusercontent.com'; //Google client ID
		$clientSecret = 'v0ArUrShMNlZB-E_DsVghE8m'; //Google client secret
		$redirectURL = base_url() . 'auth';

		//Call Google API
		$gClient = new Google_Client();
		$gClient->setApplicationName('Login');
		$gClient->setClientId($clientId);
		$gClient->setClientSecret($clientSecret);
		$gClient->setRedirectUri($redirectURL);
		$google_oauthV2 = new Google_Oauth2Service($gClient);

		if(isset($_GET['code']))
		{
			$gClient->authenticate($_GET['code']);
			$_SESSION['token'] = $gClient->getAccessToken();
			header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
		}

		if (isset($_SESSION['token'])) 
		{
			$gClient->setAccessToken($_SESSION['token']);
		}
		
		if ($gClient->getAccessToken()) {
            $data['userProfile']= $google_oauthV2->userinfo->get();
            $_SESSION['user'] = $data['userProfile'];
			//echo "<pre>";
			//print_r($userProfile);
			//die;
        } 
		else 
		{
            $url = $gClient->createAuthUrl();
		    header("Location: $url");
            exit;
        }
        
        redirect('/');
	}
}
