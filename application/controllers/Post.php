<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Post extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		$this->load->model('post_model', 'pm');

		require_once APPPATH.'third_party/src/Faker/autoload.php';
  
	}

	public function index()
	{
		$content = $this->input->post("content");
		$user_id = $this->input->post("user_id");

		$array = array(
					'user_id' =>$user_id,
					'content' =>$content
					);
		$this->pm->savepost($array);
		
		
    	$page = ($this->input->get('page')) ? $this->input->get('page') : 0;
    	$data = $this->get_posts($page);
    	$data['form_action'] = 'post';
    	$data['title_header'] = 'Your Recent Blogs';

    	$this->load->view('posts_view.php', $data);

		
	}
	public function edit()
	{
		$uri = $this->uri->segment(3);
		$data['uri'] = $uri;
		$data['form_action'] = base_url() . 'post/update/' . $uri;
		$data['result'] = $this->pm->getcontent($uri);
		$data['post'] = $data['result']['0']->content;
		echo json_encode($data);
	}

	public function update()
	{
		$uri = $this->uri->segment(3);
      	$update = array(
			'content' => $this->input->post('content'),
			);
		$this->db->where('post_id', $uri)->update('tbl_post', $update);

		$data = $this->get_posts(0);
    	$data['form_action'] = 'post';
    	$data['title_header'] = 'Your Recent Blogs';
    	$this->load->view('posts_view.php', $data);
  	}

  	public function delete()
	{
  		$id = $this->uri->segment(3);
  		$this->pm->delete($id);
  		echo 'success';
	}

	public function generate_posts()
	{
		$faker = Faker\Factory::create('en_US');

		for ($i=0; $i < 10000; $i++) {
		  $text = $faker->realText($maxNbChars = 300, $indexSize = 2);
		  $array = array(
					'user_id' =>'1',
					'content' =>$text
					);
		$this->pm->savepost($array);
		}
		echo 'Created 10000 posts';
	}

	public function get_posts($page){
		$per_page = 5;
		$id =  $_SESSION['user']['id'];
		$data['user_id'] = $id;
    	$data['posts'] =  $this->pm->getposts($page, $per_page, $id);
    	
    	$total_rows = $this->pm->numberofitems($id);
		$data['links'] = $this->pagination_lib->generate_pagination_links($total_rows, '/home');
		return $data;
	}
}
