<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends CI_Model{
	
	
	public function savepost($content){
		$this->db->insert('tbl_post', $content);
	}//end fct

	public function getcontent($id){
		return $this->db->select(
				'tp.post_id,
				 tp.user_id,
				 tp.content
				')
				->from('tbl_post tp')
				->where('post_id', $id)
				->order_by('tp.post_id', 'desc')
				->get()->result_object();
		 	$this->db->get('tbl_post');
	}//end fct

	public function getposts($start, $limit, $user_id=null){
		$this->db->select(
				'tp.post_id,
				 tp.user_id,
				 tp.content
				');
		$this->db->from('tbl_post tp');
		if ($user_id) $this->db->where('user_id', $user_id);
		$this->db->limit($limit, $start);
		$this->db->order_by('tp.post_id', 'desc');
		$query =  $this->db->get()->result_object();

		return $query;
	}//end fct
	
	
	public function delete($id){
		$this->db->where('post_id', $id)->delete('tbl_post');
	}//end fct

	
	public function numberofitems($user_id){
		$this->db->select('user_id');
		$this->db->from('tbl_post');
		if ($user_id) $this->db->where('user_id', $user_id);
		return $this->db->count_all_results();
	}//end fct
	
}
