<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">

    <title>Blogging App</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>/assets/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url()?>/assets/custom.css" rel="stylesheet">

    <!-- Sticky footer styles for this template -->
    <link href="<?php echo base_url()?>/assets/sticky-footer-navbar.css" rel="stylesheet">
    <!-- markdown -->
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/markdown-it.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/markdown-it-footnote.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/highlight.pack.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/emojify.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/lib/codemirror.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/overlay.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/xml/xml.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/markdown/markdown.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/gfm/gfm.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/javascript/javascript.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/css/css.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/htmlmixed/htmlmixed.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/lib/util/continuelist.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/rawinflate.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/markdown-editor/rawdeflate.js"></script>
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/plugins/markdown-editor/base16-light.css">
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/plugins/markdown-editor/codemirror/lib/codemirror.css">
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/plugins/markdown-editor/default.css">
    <!-- end markdown -->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>">Blogging App</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <?php 
              if(!empty($_SESSION['token'])){ ?>
              <li class="active"><a href="<?php echo base_url()?>">My Account</a></li>
              <li class="active"><a href="<?php echo base_url()?>logout">Logout</a></li>
            <?php }
            else{
            ?>
              <li class="active"><a href="<?php echo base_url()?>auth">Login with Google</a></li>
            <?php 
            }
            ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <?php echo $main_content; ?>

    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>/assets/app.js"></script>
    <script src="<?php echo base_url()?>/assets/bootstrap.min.js"></script>
    <?php if ($user_id): ?>
    <script src="<?php echo base_url()?>/assets/markdown-editor.js"></script>
<?php endif ;?>
</body></html>