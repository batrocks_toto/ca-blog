<div class="page-header">
    <div class="container">
      <h1><?php echo $title_header; ?></h1>

      <ul>
          <?php foreach($posts as $list):?>
          
            <li id="post-<?php echo $list->post_id; ?>">
              <?php echo $list->content; ?>
              <?php if($user_id): ?>
                <p>Options: <span><a href="#" class="edit-post" data-id="<?php echo $list->post_id; ?>">Edit</a></span>&nbsp;<span><a href="<?php echo base_url(); ?>post/delete/<?php echo $list->post_id; ?>" class="delete-post" data-id="<?php echo $list->post_id; ?>"> Delete</a></span></p>
              <?php endif ?>
            
            </li>
          <?php endforeach;?>
        </ul>
        <div class="paginate">
          <?php echo $links; ?>
        </div>
    </div>
  </div>