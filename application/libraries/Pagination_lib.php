<?php

/*
 * Main Library class for the templates
 * @author: mylistahan
 */

class Pagination_lib{

    /*
     * Fct that will set the values  for the template layout
     *
     * @params:
     *      $value - value to be assigned
     *      $name - name on the html
     */
    public function set_config($total_rows, $url) {
    	$CI =& get_instance();
        $CI->load->library('pagination');

		$config = array();
		$config['base_url'] = base_url(). $url;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = 5;
		$config['query_string_segment'] = 'page';
		$config['num_links'] = 4;
		$config['page_query_string'] = TRUE;
		$CI->pagination->initialize($config);
    }//endfct

    public function generate_pagination_links($total_rows, $url) {
    	$CI =& get_instance();
        $CI->load->library('pagination');
        
    	$this->set_config($total_rows, $url);
    	return $CI->pagination->create_links();
    }
    
}//endfct
/*End of File Cipagination.php*/
/*File Location: ./application/libraries/Cipagination.php*/

