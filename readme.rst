###################
What is CA Blog
###################

CA blog or Coding Avenue Blog is a SINGLE-PAGE APP​ blogging platform for my
company. Uses Google OAuth in signing in.

*******************
Server Requirements
*******************

PHP version 5.3 or newer is recommended.

Database: PHPMyAdmin

Server: Apache

************
Installation
************

Pull the repo and import the provided google.sql files found in the repo.

************
How it works
************

Generate 100000 posts
 -> run the in the browser '/posts/generate_posts'

Import DB
 -> Go to PHPMyAdmin
 -> import