$(document).ready(function() {
	console.log('SPA here!')

	$('.publish-post').submit(function(e){
		e.preventDefault()

		const $this = $(this)
		console.log($this)
		const postUrl = $this.attr('action')
		console.log('posturl: ', postUrl)

		const payload = $this.serialize()
		console.log(payload)

		$.post(postUrl, payload, function(data, status){
			console.log('data: ', data)
			console.log('status: ', status)
			$('.posts-list').html(data)
			//$('.post-textarea').val('')
			editor.setValue('')
		})
		// return false;
	})

	$(document).on('click', '.edit-post', function(e) {
		e.preventDefault()

		const $this = $(this)
		// const getUrl = $this.attr('href')
		const postID = $this.attr('data-id')
		const getUrl = `/google/post/edit/${postID}`
		console.log(getUrl)

		// const id = $this.attr('data-id')
		
		$.get(getUrl, function(data, status) {
			const $parseData = JSON.parse(data)
			console.log($parseData)
			$('.publish-post').attr('action', $parseData.form_action)
			$('.post-textarea').val($parseData.post)
			editor.setValue($parseData.post)
		})
	})

	$(document).on('click', '.delete-post', function(e) {
		e.preventDefault()

		const $this = $(this)
		// const getUrl = $this.attr('href')
		const postID = $this.attr('data-id')
		const postUrl = `/google/post/delete/${postID}`
		console.log(postUrl)

		$.post(postUrl, function(data, status) {
			$(`#post-${postID}`).remove()
		})
	})

	$(document).on('click', '.paginate > a', function(e) {
		e.preventDefault()
		console.log('paginate here!')
		const $this = $(this)
		const getUrl = $this.attr('href')
		$.get(getUrl, function(data, status) {
			const $parseData = JSON.parse(data)
			console.log($parseData)
			$('.posts-list').html($parseData.posts_view)
		})
	})
})


